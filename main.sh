#!/bin/bash

# Database program for Prices or Anything else :)
# A file saving database

function msg_err {
  printf "\033[31;1m$@ \033[0m%s\n"
}

function msg {
  printf "\033[32;1m$@\033[0m%s\n" 
}

quit=0
databasePlace="/home/$USER/Filing Database/database"
clear

while [ "$quit" == "0" ]; do
	

databaseUnit="Price"


echo "1. Display $databaseUnit"
echo "2. Set $databaseUnit"
echo "3. Delete $databaseUnit"
echo "4. List $databaseUnit"
echo "5. Quit"

echo -n "> "
read answerToFirst

if [ "$answerToFirst" == "1" ]; then
	echo ""
	echo -n "Code: "
	read readCode
	# cat "$databasePlace/$readCode"
	echo -n "$databaseUnit: " && msg "$(cat "$databasePlace/$readCode" || msg_err 'Not Set')"
fi

if [ "$answerToFirst" == "2" ]; then
	echo ""
	echo -n "Code: "
	read writeCode
	echo -n "Content: "
	read writeContent
	msg "$writeContent" | tee "$databasePlace/$writeCode" > /dev/null
fi

if [ "$answerToFirst" == "3" ]; then
	echo ""
	echo -ne "\033[31;1mCode:  \033[0m"
	# msg_err "Code: "
	read delCode
	rm "$databasePlace/$delCode"
fi

if [ "$answerToFirst" == "4" ]; then
	ls -l "$databasePlace"
fi

if [ "$answerToFirst" == "5" ]; then
	exit 0
fi


echo ""
echo ""
echo ""

done

exit 1