# Filing Database
A simple script to store data in it's file.

## How to use
Clone the project:
`cd && git clone https://gitlab.com/MM4/FilingDatabase.git && cd Filing*`
<br />
Then run the main script.<br />
`./main`

## To add...
I'm going to add an first-run code to init the script and get ready to use.<br />
Any issues or suggestions wellcome :)